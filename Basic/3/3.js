#!/usr/bin/env node
// Write a JavaScript program to get the current date.  Go to the editor
// Expected Output :
// mm-dd-yyyy, mm/dd/yyyy or dd-mm-yyyy, dd/mm/yyyy
function date_conv(f,s,t,seperator) {
  return [f,s,t].join(seperator)
}
var d = new Date();
var year = d.getUTCFullYear();
var month = d.getMonth() + 1;
var day = d.getUTCDate();
console.log(date_conv(month,day,year,"-"))
console.log(date_conv(month,day,year,"/"))
console.log(date_conv(day,month,year,"/"))
