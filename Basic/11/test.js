var inquirer = require('inquirer');

function processAnswers(answers){
  console.log("And your answers are:", answers);
}
var questions = [
{
  type: 'list',
  name: 'product',
  message: 'Which Apple product you like more?',
  choices: ['iPhone8', 'AppleWatch', 'MacBookPro', 'iPhoneX', 'iPad', 'iPhone7'],
  filter: function (val) {
      return val.toLowerCase();
  }
}];
inquirer.prompt(questions).then((answers) => {
  console.log("And your answers are:", answers);
});
