#!/usr/bin/env node
// Write a JavaScript program to convert temperatures to and from Celsius, Fahrenheit.
// Formula : c/5 = (f-32)/9 [ where c = temperature in Celsius and f = temperature in Fahrenheit ]
var inquirer = require('inquirer');
var inquirer_1 = require('inquirer');

function inspectAnswers(answers){
  console.log("And your answers are:", answers);
}

function c_2_f(c) {
  return c*9/5 + 32
}

function f_2_c(f) {
  return 5*(f-32)/9
}

var question_0 = [{
  type: 'list',
  name: 'function',
  message: 'Which type of temperature convertion do you want to perform today?',
  choices: ['celsius to fahrenheit', 'fahrenheit to celsius'],
  filter: function (val) {
      return val.toLowerCase();
  }
}]

var question_1 = [{
  type: 'input',
  name: 'Fahrenheit',
  messaage: "What the temperature in Fahrenheit? "
}]

var question_2 = [{
  type: 'input',
  name: 'Celsius',
  messaage: "What the temperature in Celsius? "
}]

inquirer.prompt(question_0)
.then(answers => {
  if (answers['function'] === 'celsius to fahrenheit') {
    inquirer_1.prompt(question_2)
    .then(answers => {
      fa = c_2_f(answers['Celsius']);
      console.log(`The temperature is ${fa} in fahrenheit.`);
    });
  } else if (answers['function'] === 'fahrenheit to celsius'){
    inquirer_1.prompt(question_1)
    .then(answers => {
      cel = f_2_c(answers['Fahrenheit']);
      console.log(`The temperature is ${cel} in celsius.`);
    });
  }
});
