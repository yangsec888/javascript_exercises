#!/usr/bin/env node
// Write a JavaScript program to check from two given integers, if one is
// positive and one is negative.
var inquirer = require('inquirer');

function chkTwo(x,y) {
  if (((x>0) && (y<0)) || ((x<0) && (y>0))) {
    return true;
  } else {
    return false;
  }
}

inquirer.prompt(
  [{
    type: 'input',
    name: 'twoNum',
    message: 'Pleae type in two numbers, separated by comma: ',
  }])
.then(answers => {
  [x,y] = answers['twoNum'].split(',');
  console.log("Check of 2 given numbers one positive one negative:", chkTwo(Number(x),Number(y)));

});
