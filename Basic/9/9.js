#!/usr/bin/env node
// Write a JavaScript program to calculate days left until next Christmas.

var d = new Date();
var year = d.getUTCFullYear();
//console.log(`Current year: ${year}`);
var t_day_utc = d.getTime();
//console.log(`today in utc: ${t_day_utc}`);
var c_day_utc = Date.UTC(year,11,25);
//console.log(`christmas in utc: ${c_day_utc}`);
var diff = Math.ceil((c_day_utc - t_day_utc) / 8.64e+7);

console.log(`There are still ${diff} days left to the Christmas`);
