#!/usr/bin/env node
// Write a JavaScript exercise to create a variable using a user-defined name.
var var1;
// user variable
var var_user = 100;
// create a variable
this[var1] = var_user;
// proof
console.log(this[var1]);
