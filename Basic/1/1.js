#!/usr/bin/env node
/* Write a JavaScript program to display the current day and time in the following format.  Go to the editor
Sample Output : Today is : Tuesday.
Current time is : 10 PM : 30 : 38 */

// Convert time string into am|pm format:  18:00:00 => 6:00:00 pm
function time_conver(timeString) {
  var H = timeString.substr(0, 2);
  var h = H % 12 || 12;
  var ampm = (H < 12 || H === 24) ? "AM" : "PM";
  return  h + ampm + timeString.substr(2, 6);
}
// Convert week day from numberic number to string:  1 => Monday
var week_day = (day) => {
  dayList = ["Sunday","Monday","Tuesday","Wednesday ","Thursday","Friday","Saturday"];
  return dayList[day];
}
// Main Program
var d = new Date();
var timestring = d.toTimeString().split(" ")[0];
var msg1 = "Today is : " + week_day(d.getDay()) + ".";
var msg2  = "Current time is: " + time_conver(d.toTimeString().split(" ")[0]);
console.log(msg1);
console.log(msg2);
