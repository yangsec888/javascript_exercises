#!/usr/bin/env node
/*Write a JavaScript program to rotate the string 'w3resource' in
right direction by periodically removing one letter from the end
of the string and attaching it to the front.
*/
function rotate(in_string) {
  let my_array=in_string.split('');
  //console.log("my_array: ", my_array);
  let my_char=my_array[my_array.length-1];
  my_array.pop();
  let out_string = my_char + my_array.join('');
  return out_string;
}

console.log(rotate("w3resource"));
