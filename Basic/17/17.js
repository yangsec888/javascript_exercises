#!/usr/bin/env node
// Write a JavaScript program to compute the absolute difference between a
// specified number and 19. Returns triple their absolute difference if the
// specified number is greater than 19.

var inquirer = require('inquirer');

inquirer.prompt(
  [{
    type: 'input',
    name: 'triDiff',
    message: 'Pleae type in a number: ',
  }])
.then(answers => {
  if (answers['triDiff'] > 19) {
    console.log("Triple difference of given number and 19 is:", 3*(answers['triDiff']-19) );
  } else {
    console.log("Difference of given number and 19 is:", answers['triDiff']-19);
  }
});
