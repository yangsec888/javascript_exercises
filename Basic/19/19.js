#!/usr/bin/env node
// Write a JavaScript program to check a given integer is within 20 of 100 or 400.

var inquirer = require('inquirer');

inquirer.prompt(
  [{
    type: 'input',
    name: 'myDif',
    message: 'Pleae type in a number: '
  }])
.then(answers => {
  if ( (Math.abs((Number(answers['myDif']) - 100)) < 20) || (Math.abs((Number(answers['myDif']) - 400)) < 20) ) {
    console.log("Given number within 20 of 100 or 400:", true);
  } else {
    console.log("Given number within 20 of 100 or 400:", false);
  }
});
