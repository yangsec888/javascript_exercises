#!/usr/bin/env node
/*
Write a JavaScript program where the program takes a random integer between 1 to 10,
the user is then prompted to input a guess number. If the user input matches
with guess number, the program will display a
message "Good Work" otherwise display a message "Not matched".
*/
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

/*
function wait(ms) {
  var start = new Date().getTime();
  var end = start;
  while(end < start + ms) {
    end = new Date().getTime();
  }
}
*/

function read_in() {
  return readline.question('Please enter a random number between 1 to 10: ');
}

function game(i)  {
  for (i=1;i<5;i++) {
    console.log(`Game start round ${i} ...`);
    readline.question('Please enter a random number between 1 to 10: ', (num) => {
      ram = Math.floor((Math.random() * 10) + 1);
      if (ram == num) {
        console.log("Good Work. Your guest ${num} is correct. ");
      } else {
        console.log(`Expected random number: ${ram} `);
        console.log(`Not matched. Your guest ${num} is incorrect. `);
      }
      //readline.close();
    });
  };
  //wait(1000);
}
//}
//game(1);
//*
game(5);

//
//readline.close();
