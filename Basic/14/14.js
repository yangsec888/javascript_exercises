#!/usr/bin/env node
// Write a JavaScript exercise to get the extension of a filename.

var filename = "index.js";

// file extension
if (filename.includes('.')) {
  var ext = filename.split('.').pop();
  // printout
  console.log(`filename extension is: ${ext}`);
} else {
  console.log(`Error file ${filename} does not have a proper file extension. `);
}
