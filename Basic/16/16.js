#!/usr/bin/env node
// Write a JavaScript program to compute the sum of the two given integers. If
// the two values are same, then returns triple their sum.

var inquirer = require('inquirer');

function triSum(x,y) {
  if (x==y) {
    return x+y;
  } else {
    return 3*(x+y);
  }
}

inquirer.prompt(
  [{
    type: 'input',
    name: 'twoNum',
    message: 'Pleae type in two numbers, separated by comma: ',
  }])
.then(answers => {
  [x,y] = answers['twoNum'].split(',');
  console.log("Tri-sum of given numbers is:", triSum(Number(x),Number(y)));

});
