#!/usr/bin/env node
// Write a JavaScript program to check two given numbers and return true if one
// of the number is 50 or if their sum is 50
var inquirer = require('inquirer');

function comTwo(x,y) {
  if ((x==50) || (y==50) || (x+y==50)) {
    return true;
  } else {
    return false;
  }
}

inquirer.prompt(
  [{
    type: 'input',
    name: 'twoNum',
    message: 'Pleae type in two numbers, separated by comma: ',
  }])
.then(answers => {
  [x,y] = answers['twoNum'].split(',');
  console.log("Computing of 2 given numbers is:", comTwo(Number(x),Number(y)));

});
