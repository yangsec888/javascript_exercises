#!/usr/bin/env node
// Write a JavaScript program to calculate multiplication and division of two numbers (input from user).

console.log(`Multiplication of your numbers ${process.argv[2]} and ${process.argv[3]} is: ${process.argv[2] * process.argv[3]}`);
console.log(`Division of your numbers ${process.argv[2]} and ${process.argv[3]} is: ${process.argv[2] / process.argv[3]}`);
