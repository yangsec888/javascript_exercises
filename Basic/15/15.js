#!/usr/bin/env node
// Write a JavaScript program to get the difference between a given number and
// 13, if the number is greater than 13 return double the absolute difference.

var inquirer = require('inquirer');

inquirer.prompt(
  [{
    type: 'input',
    name: 'givenNumber',
    message: 'Pleae type in a number: ',
  }])
.then(answers => {
  if (answers['givenNumber'] > 13) {
    console.log("Double difference of given number and 13 is:", 2*(answers['givenNumber']-13) );
  } else {
    console.log("Difference of given number and 13 is:", answers['givenNumber']-13);
  }
});
